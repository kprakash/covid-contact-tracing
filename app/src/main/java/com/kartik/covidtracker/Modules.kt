package com.kartik.covidtracker

import androidx.room.Room
import com.kartik.covidtracker.data.ApplicationPreferences
import com.kartik.covidtracker.data.repositories.contacts.ContactsDatabase
import com.kartik.covidtracker.data.repositories.covid.CovidUserResultsDatabase
import com.kartik.covidtracker.data.repositories.firestore.CloudFirestore
import com.kartik.covidtracker.data.repositories.firestore.CloudFirestoreImpl
import com.kartik.covidtracker.data.repositories.keys.DeviceKeysDatabase
import com.kartik.covidtracker.service.ble.*
import com.kartik.covidtracker.service.keys.DeviceKeysInteractor
import com.kartik.covidtracker.service.keys.DeviceKeysInteractorImpl
import com.kartik.covidtracker.service.tracker.ReportingService
import com.kartik.covidtracker.service.tracker.ReportingServiceImpl
import com.kartik.covidtracker.ui.main.reporting.MainFragmentPresenter
import com.kartik.covidtracker.ui.main.reporting.MainFragmentPresenterImpl
import com.kartik.covidtracker.ui.main.stats.StatsFragmentPresenter
import com.kartik.covidtracker.ui.main.stats.StatsFragmentPresenterImpl
import com.kartik.covidtracker.ui.splash.SplashPresenter
import com.kartik.covidtracker.ui.splash.SplashPresenterImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val splashModule = module {
    factory<SplashPresenter> { SplashPresenterImpl() }
}

val mainActivityModule = module {
    factory<MainFragmentPresenter> {
        MainFragmentPresenterImpl(
            covidDetailsDao = get(),
            reportingService = get()
        )
    }
}

val statsModule = module {
    factory<StatsFragmentPresenter> {
        StatsFragmentPresenterImpl(
            database = get(),
            contactsDao = get()
        )
    }
}

val bleModule = module {
    single<BleBroadcaster> { BleBroadcasterImpl(
        keysInteractor = get()
    ) }
    single<BleScanner> { BleScannerImpl(contactsDao = get()) }
    single<BleService> { BleServiceImpl(advertiser = get(), scanner = get()) }
}

val storageModule = module {
    single { ApplicationPreferences(androidApplication()) }

    single {
        Room.databaseBuilder(
            androidApplication(),
            ContactsDatabase::class.java,
            "device-contacts.db"
        ).build()
    }
    factory { get<ContactsDatabase>().dao() }

    single {
        Room.databaseBuilder(
            androidApplication(),
            DeviceKeysDatabase::class.java,
            "device-keys.db"
        ).build()
    }
    factory { get<DeviceKeysDatabase>().dao() }

    single {
        Room.databaseBuilder(
            androidApplication(),
            CovidUserResultsDatabase::class.java,
            "covid-details.db"
        ).build()
    }
    factory { get<CovidUserResultsDatabase>().dao() }

    single<CloudFirestore> { CloudFirestoreImpl(
        prefs = get()
    ) }
}

val deviceKeysModule = module {
    single<DeviceKeysInteractor> {
        DeviceKeysInteractorImpl(repo = get())
    }
}

val reportingModule = module {
    single<ReportingService> {
        ReportingServiceImpl(
            dao = get(),
            database = get(),
            keysInteractor = get()
        )
    }
}
