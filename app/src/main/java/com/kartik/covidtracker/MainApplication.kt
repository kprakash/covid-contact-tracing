package com.kartik.covidtracker

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.PowerManager
import com.google.firebase.ktx.Firebase
import com.google.firebase.ktx.app
import com.kartik.covidtracker.service.TrackerForegroundService
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        initializeApp()
    }

    @SuppressLint("WakelockTimeout")
    fun initializeApp() {
        (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
            newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "CovidTracker:WakelockTag").apply {
                acquire()
            }
        }

        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(
                splashModule,
                bleModule,
                storageModule,
                deviceKeysModule,
                mainActivityModule,
                reportingModule,
                statsModule
            )
        }
    }

    fun startForegroundService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(
                Intent(
                    this, TrackerForegroundService::class.java
                )
            )
        } else {
            startService(
                Intent(
                    this, TrackerForegroundService::class.java
                )
            )
        }
    }

    companion object {
        private lateinit var instance: MainApplication
        fun instance(): MainApplication =
            instance
    }
}
