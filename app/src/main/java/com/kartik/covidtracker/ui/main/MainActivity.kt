package com.kartik.covidtracker.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.covidtracker.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.kartik.covidtracker.ui.main.faq.FAQFragment
import com.kartik.covidtracker.ui.main.reporting.MainFragment
import com.kartik.covidtracker.ui.main.stats.StatsFragment

class MainActivity : AppCompatActivity() {

    private var currentMenuItem: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, StatsFragment.newInstance())
                .commitNow()
        }
        window.statusBarColor = resources.getColor(R.color.background)
        window.navigationBarColor = resources.getColor(R.color.background)

        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(bottomNavigationListener)
    }

    private val bottomNavigationListener = BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.navigation_stats -> {
                if (currentMenuItem != R.id.navigation_stats) {
                    currentMenuItem = R.id.navigation_stats
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, StatsFragment.newInstance())
                        .commitNow()
                }
                true
            }
            R.id.navigation_reporting -> {
                if (currentMenuItem != R.id.navigation_reporting) {
                    currentMenuItem = R.id.navigation_reporting
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, MainFragment.newInstance())
                        .commitNow()
                }
                true
            }

            R.id.navigation_faq -> {
                if (currentMenuItem != R.id.navigation_faq) {
                    currentMenuItem = R.id.navigation_faq
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, FAQFragment.newInstance())
                        .commitNow()
                }
                true
            }
            else -> false
        }
    }
}
