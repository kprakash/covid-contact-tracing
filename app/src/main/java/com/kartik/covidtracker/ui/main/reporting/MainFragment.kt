package com.kartik.covidtracker.ui.main.reporting

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.covidtracker.R
import org.koin.android.ext.android.inject

class MainFragment : Fragment() {

    companion object {
        fun newInstance() =
            MainFragment()
    }

    private val presenter: MainFragmentPresenter by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.findViewById<Button>(R.id.report_symptoms_button)
            ?.setOnClickListener { presenter.onSymptomsSubmitted(this) }

        view?.findViewById<Button>(R.id.report_test_result)
            ?.setOnClickListener { presenter.onSubmitTestResult(this) }

        presenter.setupLastSubmittedValue(this)
    }

}
