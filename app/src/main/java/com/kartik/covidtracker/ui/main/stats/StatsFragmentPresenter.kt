package com.kartik.covidtracker.ui.main.stats

import android.graphics.Paint
import android.os.Build
import android.text.SpannableString
import com.example.covidtracker.R
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.kartik.covidtracker.data.models.CovidSymptoms
import com.kartik.covidtracker.data.models.CovidTestResult
import com.kartik.covidtracker.data.repositories.contacts.ContactsDao
import com.kartik.covidtracker.data.repositories.firestore.CloudFirestore
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.Duration
import org.threeten.bp.Instant

interface StatsFragmentPresenter {
    fun onCreate(fragment: StatsFragment)
    fun onDestroy()
}

class StatsFragmentPresenterImpl(
    private val database: CloudFirestore,
    private val contactsDao: ContactsDao
) : StatsFragmentPresenter {
    private val disposable = CompositeDisposable()
    override fun onCreate(fragment: StatsFragment) {
        setupSymptomsChart(fragment)
        setupTestResultChart(fragment)
    }

    override fun onDestroy() {
        disposable.clear()
    }

    private fun setupTestResultChart(fragment: StatsFragment) {
        val chartView = fragment.view?.findViewById<PieChart>(R.id.test_result_chart_view)
        chartView?.description = Description().apply { this.text = "" }
        val totalContacts = ArrayList<String>()

        Single.create<List<CovidTestResult>> {
            val duration = Duration.ofDays(15)
            val now = Instant.now()
            val contacts = contactsDao.getAllContacts()
                .filter { Duration.between(now, it.lastContactedTime) <= duration }
                .map { it.deviceId }
                .also { totalContacts.addAll(it) }

            val symptoms = database.readTestResults(contacts).blockingGet()
            it.onSuccess(symptoms)
        }.subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { results ->
                    val keysWithPositiveResult = results
                        .filter { it.testResult }
                        .flatMap { it.keys?.split(CovidSymptoms.dbSeperator) ?: emptyList() }

                    val noResult = totalContacts.filter {
                        it !in keysWithPositiveResult
                    }

                    if (noResult.isEmpty() && keysWithPositiveResult.isEmpty()) {
                        fragment.activity?.runOnUiThread {
                            chartView?.setNoDataText("You haven't come in contact with anyone")
                        }
                    } else {
                        val data = ArrayList<PieEntry>()
                        data.add(PieEntry(results.size.toFloat(), "Positive"))
                        data.add(PieEntry(noResult.size.toFloat(), "Not Reported"))

                        val dataSet = PieDataSet(data, "")
                        val colors = ArrayList<Int>()

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            colors.add(fragment.resources.getColor(R.color.negative_red, null))
                            colors.add(fragment.resources.getColor(R.color.positive_green, null))
                        } else {
                            colors.add(fragment.resources.getColor(R.color.negative_red))
                            colors.add(fragment.resources.getColor(R.color.positive_green))
                        }

                        dataSet.colors = colors
                        dataSet.setDrawIcons(false)
                        dataSet.sliceSpace = 3f
                        dataSet.valueTextSize = 18f
                        dataSet.isValueLineVariableLength = true

                        val pieData = PieData(dataSet)

                        fragment.activity?.runOnUiThread {
                            chartView?.data = pieData
                            chartView?.description = Description().apply {
                                this.text = "COVID-19 Test Results"
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    this.textColor = fragment.resources.getColor(R.color.symptom_colors, null)
                                } else {
                                    this.textColor = fragment.resources.getColor(R.color.symptom_colors)
                                }
                                this.textSize = 12f
                            }
                            chartView?.invalidate()
                        }
                    }
                }
            )
            .also { disposable.add(it) }
    }

    private fun setupSymptomsChart(fragment: StatsFragment) {
        val chartView = fragment.view?.findViewById<PieChart>(R.id.symptoms_chart_view)
        val totalContacts = ArrayList<String>()
        chartView?.description = Description().apply { this.text = "" }

        Single.create<List<CovidSymptoms>> {
            val duration = Duration.ofDays(15)
            val now = Instant.now()
            val contacts = contactsDao.getAllContacts()
                .filter { Duration.between(now, it.lastContactedTime) <= duration }
                .map { it.deviceId }
                .also { totalContacts.addAll(it) }

            val symptoms = database.readSymptoms(contacts).blockingGet()
            it.onSuccess(symptoms)
        }.subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { symptoms ->
                    val keysWithSymptoms = symptoms.flatMap { it.keys?.split(CovidSymptoms.dbSeperator) ?: emptyList() }
                    val noSymptoms = totalContacts.filter {
                        it !in keysWithSymptoms
                    }

                    if (keysWithSymptoms.isEmpty() && noSymptoms.isEmpty()) {
                        fragment.activity?.runOnUiThread {
                            chartView?.setNoDataText("You haven't come in contact with anyone")
                        }
                    } else {
                        val data = ArrayList<PieEntry>()
                        data.add(PieEntry(symptoms.size.toFloat(), "Symptoms"))
                        data.add(PieEntry(noSymptoms.size.toFloat(), "Not Reported"))

                        val dataSet = PieDataSet(data, "")
                        val colors = ArrayList<Int>()

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            colors.add(fragment.resources.getColor(R.color.negative_red, null))
                            colors.add(fragment.resources.getColor(R.color.positive_green, null))
                        } else {
                            colors.add(fragment.resources.getColor(R.color.negative_red))
                            colors.add(fragment.resources.getColor(R.color.positive_green))
                        }

                        dataSet.colors = colors
                        dataSet.setDrawIcons(false)
                        dataSet.sliceSpace = 3f
                        dataSet.valueTextSize = 18f
                        dataSet.isValueLineVariableLength = true

                        val pieData = PieData(dataSet)

                        fragment.activity?.runOnUiThread {
                            chartView?.data = pieData
                            chartView?.description = Description().apply {
                                this.text = "COVID-19 Symptoms"
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    this.textColor = fragment.resources.getColor(R.color.symptom_colors, null)
                                } else {
                                    this.textColor = fragment.resources.getColor(R.color.symptom_colors)
                                }
                                this.textSize = 12f
                            }
                            chartView?.invalidate()
                        }
                    }
                }
            )
            .also { disposable.add(it) }
    }
}