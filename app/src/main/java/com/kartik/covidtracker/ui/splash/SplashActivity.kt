package com.kartik.covidtracker.ui.splash

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.covidtracker.R
import com.kartik.covidtracker.MainApplication
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import java.util.concurrent.TimeUnit

const val PERM_REQUEST_CODE = 1
const val REQUEST_ENABLE_BT = 2

class SplashActivity : Activity() {

    private val requiredPermissions = arrayOf(
        Manifest.permission.BLUETOOTH,
        Manifest.permission.BLUETOOTH_ADMIN,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    private val presenter: SplashPresenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)

        window.statusBarColor = resources.getColor(R.color.background)
        window.navigationBarColor = resources.getColor(R.color.background)

        checkPermissionsAndBle()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERM_REQUEST_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionsAndBle()
                }
            }

            REQUEST_ENABLE_BT -> {
                checkPermissionsAndBle()
            }
        }
    }

    private fun checkPermissionsAndBle() {
        if (shouldRequestPermissions()) {
            ActivityCompat.requestPermissions(
                this,
                requiredPermissions,
                PERM_REQUEST_CODE
            )
        } else {
            permissionGranted()
        }
    }

    private fun permissionGranted() {
        checkBleEnabled()
    }

    private fun enableBle() {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        ActivityCompat.startActivityForResult(this, enableBtIntent, REQUEST_ENABLE_BT, null)
    }

    private fun checkBleEnabled() {
        if (!presenter.isBleEnabled()) {
            enableBle()
        } else {
            presenter.startAllServices(this)
        }
    }

    private fun shouldRequestPermissions(): Boolean {
        requiredPermissions.forEach {
            if (ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED) {
                return true
            }
        }

        return false
    }
}