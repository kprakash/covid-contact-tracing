package com.kartik.covidtracker.ui.main.reporting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import com.example.covidtracker.R

class CovidTestResultFragment : Fragment() {

    companion object {
        fun newInstance() =
            CovidTestResultFragment()
    }

    lateinit var positiveButton: RadioButton
    lateinit var negativeButton: RadioButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.covid_test_result_fragment, container, false)
        positiveButton = view.findViewById(R.id.positive_radio_button)
        negativeButton = view.findViewById(R.id.negative_radio_button)
        return view
    }
}