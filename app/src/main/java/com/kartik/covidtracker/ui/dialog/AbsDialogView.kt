package com.kartik.covidtracker.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.example.covidtracker.R

abstract class AbsDialogView : DialogFragment()

class DialogView : AbsDialogView() {
    var bodyFragment: Fragment? = null

    var positiveButtonListener: View.OnClickListener? = null
    var negativeButtonListener: View.OnClickListener? = null

    var text: String? = null

    companion object {
        fun newInstance(): DialogView { return DialogView() }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.dialog_view, container, false)
        val textView = view.findViewById<TextView>(R.id.dialog_text)
        val positiveButton = view.findViewById<Button>(R.id.dialog_positive_button)
        val negativeButton = view.findViewById<Button>(R.id.dialog_negative_button)

        text?.let { textView.text = it }
        positiveButtonListener?.let { positiveButton.setOnClickListener(it) }
        negativeButtonListener?.let { negativeButton.setOnClickListener(it) }

        bodyFragment?.let {
            textView.visibility = View.GONE
            view.findViewById<View>(R.id.body_fragment).visibility = View.VISIBLE
            childFragmentManager
                .beginTransaction()
                .replace(R.id.body_fragment, it, "CONTENT_FRAGMENT")
                .commit()
        }

        return view
    }

    override fun onStart() {
        super.onStart()

        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
}