package com.kartik.covidtracker.ui.main.reporting

import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import com.example.covidtracker.R
import com.kartik.covidtracker.data.models.CovidSymptoms
import com.kartik.covidtracker.data.models.CovidTestResult
import com.kartik.covidtracker.data.repositories.covid.CovidUserResultsDao
import com.kartik.covidtracker.service.tracker.ReportingService
import com.kartik.covidtracker.ui.dialog.DialogView
import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import java.lang.IllegalStateException

interface MainFragmentPresenter {
    fun onSymptomsSubmitted(fragment: MainFragment)
    fun onSubmitTestResult(fragment: MainFragment)
    fun setupLastSubmittedValue(fragment: MainFragment)
}

class MainFragmentPresenterImpl(
    private val covidDetailsDao: CovidUserResultsDao,
    private val reportingService: ReportingService
) : MainFragmentPresenter {
    override fun setupLastSubmittedValue(fragment: MainFragment) {
        val lastUpdatedView = fragment.view?.findViewById<TextView>(R.id.last_updated_view)

        var disposable: Disposable? = null
        Completable.complete()
            .subscribeOn(Schedulers.io())
            .subscribe {
                val lastValue = covidDetailsDao.getSymptomLastSubmittedValue()
                    .let { it?.timestamp?.atZone(ZoneId.systemDefault())?.toLocalDateTime() }

                lastValue?.let {
                    fragment.activity?.runOnUiThread {
                        val ts = it.toString().split("T")
                        lastUpdatedView?.text = "Last submitted at: ".plus(ts[0].plus(" ${ts[1].split(".")[0]}"))
                    }
                }
                disposable?.dispose()
            }.also { disposable = it }
    }

    override fun onSymptomsSubmitted(fragment: MainFragment) {
        val symptoms = getAllSymptoms(fragment)

        if (symptoms.isEmpty()) {
            Toast.makeText(fragment.context, "No Symptoms selected", Toast.LENGTH_LONG).show()
        } else {
            val dialog = DialogView.newInstance()
            dialog.text = fragment.resources.getString(R.string.symptoms_submitted_text)
            dialog.positiveButtonListener = View.OnClickListener {
                saveCovidDetails(
                    result = null,
                    fragment = fragment,
                    symptoms = symptoms
                )
                dialog.dismissAllowingStateLoss()
            }
            dialog.negativeButtonListener = View.OnClickListener {
                dialog.dismissAllowingStateLoss()
            }

            fragment.fragmentManager?.let {
                dialog.show(it, "SYMPTOM_SUBMITTED")
            }
        }
    }

    override fun onSubmitTestResult(fragment: MainFragment) {
        val bodyFragment =
            CovidTestResultFragment.newInstance()
        val dialog = DialogView.newInstance()
        dialog.bodyFragment = bodyFragment
        dialog.positiveButtonListener = View.OnClickListener {
            when {
                bodyFragment.positiveButton.isChecked -> {
                    saveCovidDetails(
                        result = true,
                        fragment = fragment,
                        symptoms = null
                    )
                }

                bodyFragment.negativeButton.isChecked -> {
                    saveCovidDetails(
                        result = false,
                        fragment = fragment,
                        symptoms = null
                    )
                }
                bodyFragment.positiveButton.isChecked && bodyFragment.negativeButton.isChecked -> throw IllegalStateException("")
                else -> {
                    Toast.makeText(fragment.context, "Please select test result", Toast.LENGTH_LONG).show()
                    return@OnClickListener
                }
            }
            dialog.dismissAllowingStateLoss()
        }
        dialog.negativeButtonListener = View.OnClickListener {
            dialog.dismissAllowingStateLoss()
        }

        fragment.fragmentManager?.let {
            dialog.show(it, "SYMPTOM_SUBMITTED")
        }

    }

    private fun getAllSymptoms(fragment: MainFragment): List<String> {
        val symptoms = ArrayList<String>()

        fragment.view?.let {
            val views = ArrayList<CheckBox>().apply {
                add(it.findViewById(R.id.fever_checkbox))
                add(it.findViewById(R.id.cough_checkbox))
                add(it.findViewById(R.id.breath_checkbox))
                add(it.findViewById(R.id.fatigue_checkbox))
                add(it.findViewById(R.id.aches_checkbox))
                add(it.findViewById(R.id.headache_checkbox))
                add(it.findViewById(R.id.smell_checkbox))
                add(it.findViewById(R.id.sore_throat_checkbox))
                add(it.findViewById(R.id.congestion_throat_checkbox))
                add(it.findViewById(R.id.nausea_checkbox))
                add(it.findViewById(R.id.diarrhea_checkbox))
            }

            views
                .filter { it.isChecked }
                .forEach { symptoms.add(it.text.toString()) }
        }
        return symptoms
    }

    private fun saveCovidDetails(
        result: Boolean?,
        symptoms: List<String>?,
        fragment: MainFragment
    ) {
        var disposable: Disposable? = null
        Completable
            .complete()
            .subscribeOn(Schedulers.io())
            .subscribe {
                symptoms?.joinToString(CovidSymptoms.dbSeperator)?.let { stringSymptoms ->
                    val entity = CovidSymptoms(
                        timestamp = Instant.now(),
                        symptoms = stringSymptoms
                    )
                    covidDetailsDao.save(entity)
                }
                result?.let { testResult ->
                    val entity = CovidTestResult(
                        timestamp = Instant.now(),
                        testResult = testResult
                    )
                    covidDetailsDao.save(entity)
                }

                reportingService.forceSync()
                    .subscribe()

                fragment.activity?.runOnUiThread {
                    Toast.makeText(fragment.context, "Saved", Toast.LENGTH_SHORT).show()
                    setupLastSubmittedValue(fragment)
                }
                disposable?.dispose()
            }.also { disposable = it  }
    }
}