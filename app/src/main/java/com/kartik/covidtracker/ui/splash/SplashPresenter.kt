package com.kartik.covidtracker.ui.splash

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import com.kartik.covidtracker.MainApplication
import com.kartik.covidtracker.ui.main.MainActivity
import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

interface SplashPresenter {
    fun startAllServices(activity: SplashActivity)
    fun isBleEnabled(): Boolean
}

class SplashPresenterImpl : SplashPresenter {

    override fun isBleEnabled(): Boolean {
        return BluetoothAdapter.getDefaultAdapter().isEnabled
    }

    override fun startAllServices(activity: SplashActivity) {
        MainApplication.instance().startForegroundService()

        var disposable: Disposable? = null
        Completable.complete()
            .delay(2, TimeUnit.SECONDS)
            .subscribe {
                activity.startActivity(Intent(activity, MainActivity::class.java))
                activity.finish()
                disposable?.dispose()
            }.also { disposable = it }
    }
}