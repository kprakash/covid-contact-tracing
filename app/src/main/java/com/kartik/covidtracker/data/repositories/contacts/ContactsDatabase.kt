package com.kartik.covidtracker.data.repositories.contacts

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kartik.covidtracker.data.TimeInstantConverter
import com.kartik.covidtracker.data.models.Contact

@Database(entities = [Contact::class], version = 1, exportSchema = false)
@TypeConverters(TimeInstantConverter::class)
abstract class ContactsDatabase : RoomDatabase() {
    abstract fun dao(): ContactsDao
}