package com.kartik.covidtracker.data.repositories.keys

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kartik.covidtracker.data.TimeInstantConverter
import com.kartik.covidtracker.data.UUIDConverter
import com.kartik.covidtracker.data.models.DeviceKey

@Database(entities = [DeviceKey::class], version = 1, exportSchema = false)
@TypeConverters(TimeInstantConverter::class, UUIDConverter::class)
abstract class DeviceKeysDatabase : RoomDatabase() {
    abstract fun dao(): DeviceKeysDao
}