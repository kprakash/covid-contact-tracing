package com.kartik.covidtracker.data

import android.content.Context
import android.content.SharedPreferences

class ApplicationPreferences(
    private val context: Context
) {
    private enum class PrefKeys {
        SYMPTOMS_DOC_ID,
        TEST_DOC_ID
    }

    private val appPrefs: SharedPreferences = context.getSharedPreferences("application", Context.MODE_PRIVATE)

    fun saveSymptomsDocId(id: String) {
        appPrefs
            .edit()
            .putString(PrefKeys.SYMPTOMS_DOC_ID.name, id.toString())
            .apply()
    }

    fun getSymptomsDocId(): String? {
        return appPrefs
            .getString(PrefKeys.SYMPTOMS_DOC_ID.name, null)
    }

    fun saveTestDocId(id: String) {
        appPrefs
            .edit()
            .putString(PrefKeys.TEST_DOC_ID.name, id.toString())
            .apply()
    }

    fun getTestDocId(): String? {
        return appPrefs
            .getString(PrefKeys.TEST_DOC_ID.name, null)
    }
}