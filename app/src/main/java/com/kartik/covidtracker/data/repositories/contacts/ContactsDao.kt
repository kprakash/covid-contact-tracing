package com.kartik.covidtracker.data.repositories.contacts

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kartik.covidtracker.data.models.Contact

@Dao
interface ContactsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(contact: Contact)

    @Query("SELECT * from contacts")
    fun getAllContacts(): List<Contact>
}