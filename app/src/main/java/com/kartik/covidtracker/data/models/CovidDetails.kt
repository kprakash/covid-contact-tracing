package com.kartik.covidtracker.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity(tableName = "covid_user_symptom")
data class CovidSymptoms(
    @PrimaryKey
    @ColumnInfo(name = "timestamp")
    val timestamp: Instant,

    @ColumnInfo(name = "symptoms")
    val symptoms: String,

    @ColumnInfo(name = "keys")
    val keys: String? = null
) {
    companion object {
        val dbSeperator = "$$$$"
    }
}

@Entity(tableName = "covid_user_test_result")
data class CovidTestResult(
    @PrimaryKey
    @ColumnInfo(name = "timestamp")
    val timestamp: Instant,

    @ColumnInfo(name = "test_result")
    val testResult: Boolean,

    @ColumnInfo(name = "keys")
    val keys: String? = null
) {
    companion object {
        val dbSeperator = "$$$$"
    }
}
