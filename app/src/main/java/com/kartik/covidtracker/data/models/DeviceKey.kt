package com.kartik.covidtracker.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.Instant
import java.util.UUID

@Entity(tableName = "device_keys")
data class DeviceKey(
    @PrimaryKey
    @ColumnInfo(name = "device_key")
    val deviceKey: UUID,

    @ColumnInfo(name = "created_at")
    val createdAt: Instant
)