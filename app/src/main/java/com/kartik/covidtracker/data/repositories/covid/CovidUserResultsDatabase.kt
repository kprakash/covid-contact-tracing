package com.kartik.covidtracker.data.repositories.covid

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kartik.covidtracker.data.TimeInstantConverter
import com.kartik.covidtracker.data.models.CovidSymptoms
import com.kartik.covidtracker.data.models.CovidTestResult

@Database(entities = [CovidSymptoms::class, CovidTestResult::class], version = 1, exportSchema = false)
@TypeConverters(TimeInstantConverter::class)
abstract class CovidUserResultsDatabase : RoomDatabase() {
    abstract fun dao(): CovidUserResultsDao
}