package com.kartik.covidtracker.data.repositories.covid

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.kartik.covidtracker.data.models.CovidSymptoms
import com.kartik.covidtracker.data.models.CovidTestResult

@Dao
interface CovidUserResultsDao {

    @Insert
    fun save(symptoms: CovidSymptoms)

    @Insert
    fun save(testResult: CovidTestResult)

    @Query("SELECT * from covid_user_symptom ORDER BY timestamp DESC LIMIT 1")
    fun getSymptomLastSubmittedValue(): CovidSymptoms?

    @Query("SELECT * from covid_user_test_result ORDER BY timestamp DESC LIMIT 1")
    fun getLastSubmittedTestResult(): CovidTestResult?
}