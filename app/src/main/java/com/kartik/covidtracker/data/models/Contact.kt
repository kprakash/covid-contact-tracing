package com.kartik.covidtracker.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity(tableName = "contacts")
data class Contact(
    @PrimaryKey
    @ColumnInfo(name = "device_id")
    val deviceId: String,

    @ColumnInfo(name = "last_contacted_time")
    val lastContactedTime: Instant,

    @ColumnInfo(name = "rssi")
    val rssi: Int
)
