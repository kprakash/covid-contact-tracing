package com.kartik.covidtracker.data.repositories.keys

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kartik.covidtracker.data.models.DeviceKey

@Dao
interface DeviceKeysDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun save(key: DeviceKey)

    @Query("SELECT * FROM device_keys")
    fun readAll(): List<DeviceKey>

    @Query("SELECT * FROM device_keys ORDER BY created_at DESC LIMIT 1")
    fun getLatestKey(): DeviceKey?

    @Delete
    fun delete(key: DeviceKey)
}