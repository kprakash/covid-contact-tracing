package com.kartik.covidtracker.data.repositories.firestore

import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.kartik.covidtracker.data.ApplicationPreferences
import com.kartik.covidtracker.data.models.CovidSymptoms
import com.kartik.covidtracker.data.models.CovidTestResult
import io.reactivex.Single
import org.threeten.bp.Instant
import java.util.UUID

interface CloudFirestore {
    fun uploadSymptoms(
        keys: List<UUID>,
        symptoms: CovidSymptoms
    )

    fun uploadTestResult(
        keys: List<UUID>,
        result: CovidTestResult
    )

    fun readSymptoms(keys: List<String>): Single<List<CovidSymptoms>>
    fun readTestResults(keys: List<String>): Single<List<CovidTestResult>>
}

class CloudFirestoreImpl(
    private val prefs: ApplicationPreferences
) : CloudFirestore {
    override fun uploadSymptoms(
        keys: List<UUID>,
        symptoms: CovidSymptoms
    ) {
        val db = Firebase.firestore
        val keyStrings = keys.map { it.toString() }

        val symptom = hashMapOf(
            "timestamp" to symptoms.timestamp.toEpochMilli(),
            "symptoms" to symptoms.symptoms.split(CovidSymptoms.dbSeperator),
            "keys" to keyStrings
        )
        val collection = db.collection("covid-symptoms")

        if (!symptomsDocIdExists()) {
            collection
                .add(symptom)
                .addOnSuccessListener {
                    prefs.saveSymptomsDocId(it.id)
                }
        } else {
            collection
                .document(prefs.getSymptomsDocId()!!)
                .update(symptom)
                .addOnSuccessListener { }
        }
    }

    override fun uploadTestResult(keys: List<UUID>, testResults: CovidTestResult) {
        val db = Firebase.firestore
        val ketStrings = keys.map { it.toString() }

        val result = hashMapOf(
            "timestamp" to testResults.timestamp.toEpochMilli(),
            "test_result" to testResults.testResult,
            "keys" to ketStrings
        )
        val collection = db.collection("covid-test-results")

        if (!testDocIdExists()) {
            collection
                .add(result)
                .addOnSuccessListener {
                    prefs.saveTestDocId(it.id)
                }
        } else {
            collection
                .document(prefs.getTestDocId()!!)
                .set(result)
                .addOnSuccessListener { }
                .addOnFailureListener { }
        }
    }

    override fun readTestResults(
        keys: List<String>
    ): Single<List<CovidTestResult>> {
        return Single.create { emitter ->
            if (keys.isEmpty()) {
                emitter.onSuccess(emptyList())
            } else {
                val db = Firebase.firestore
                val testResultCollection = db.collection("covid-test-results")
                testResultCollection.whereArrayContainsAny("keys", keys)
                    .get()
                    .addOnSuccessListener { result ->
                        val details = result?.let {
                            it.map { document ->
                                CovidTestResult(
                                    timestamp = Instant.ofEpochMilli(document["timestamp"] as Long),
                                    testResult = document["test_result"] as Boolean,
                                    keys = (document["keys"] as List<String>).joinToString(CovidTestResult.dbSeperator)
                                )
                            }
                        }
                        emitter.onSuccess(details ?: emptyList())
                    }
                    .addOnFailureListener { emitter.onError(it) }
            }
        }
    }

    override fun readSymptoms(
        keys: List<String>
    ): Single<List<CovidSymptoms>> {
        return Single.create { emitter ->
            if (keys.isEmpty()) {
                emitter.onSuccess(emptyList())
            } else {
                val db = Firebase.firestore
                val symptomsCollection = db.collection("covid-symptoms")
                symptomsCollection.whereArrayContainsAny("keys", keys)
                    .get()
                    .addOnSuccessListener { result ->
                        val details = result?.let {
                            it.map { document ->
                                CovidSymptoms(
                                    timestamp = Instant.ofEpochMilli(document["timestamp"] as Long),
                                    symptoms = (document["symptoms"] as List<String>).joinToString(CovidSymptoms.dbSeperator),
                                    keys = (document["keys"] as List<String>).joinToString(CovidSymptoms.dbSeperator)
                                )
                            }
                        }
                        emitter.onSuccess(details ?: emptyList())
                    }
                    .addOnFailureListener { emitter.onError(it) }
            }
        }
    }

    private fun symptomsDocIdExists(): Boolean {
        return prefs.getSymptomsDocId() != null
    }

    private fun testDocIdExists(): Boolean {
        return prefs.getTestDocId() != null
    }
}