package com.kartik.covidtracker.data

import androidx.room.TypeConverter
import org.threeten.bp.Instant
import java.util.UUID

class TimeInstantConverter {
    @TypeConverter
    fun instantToLong(value: Instant?): Long? {
        return value?.toEpochMilli()
    }

    @TypeConverter
    fun longToInstant(value: Long?): Instant? {
        return value?.let { Instant.ofEpochMilli(it) }
    }
}

class UUIDConverter {
    @TypeConverter
    fun uuidToString(value: UUID?): String? {
        return value?.toString()
    }

    @TypeConverter
    fun stringToUUID(value: String?): UUID? {
        return value?.let { UUID.fromString(it) }
    }
}