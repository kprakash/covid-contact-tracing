package com.kartik.covidtracker.service.ble

import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.os.Build
import android.os.ParcelUuid
import android.util.Log
import com.kartik.covidtracker.service.keys.DeviceKeysInteractor
import io.reactivex.Completable
import io.reactivex.CompletableEmitter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.IllegalStateException

interface BleBroadcaster {
    fun startBroadcasting(): Completable
    fun stopBroadcasting()
}

class BleBroadcasterImpl(
    private val advertiser: BluetoothLeAdvertiser = BluetoothAdapter.getDefaultAdapter().bluetoothLeAdvertiser,
    private val keysInteractor: DeviceKeysInteractor
) : BleBroadcaster {

    private val disposables = CompositeDisposable()

    override fun startBroadcasting(): Completable {
        setupKeyChangeListener()
        return startAdvertiser()
    }

    private fun setupKeyChangeListener() {
        keysInteractor.keyChangeListener()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe {
                if (::callback.isInitialized) {
                    advertiser.stopAdvertising(callback)
                    startAdvertiser().subscribe()
                }

            }.also { disposables.add(it) }
    }

    private fun startAdvertiser(): Completable {
        return Completable.create {
            callback = getCallback(it)

            val settings = AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER)
                .setConnectable(false)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_ULTRA_LOW)
                .setTimeout(0)
                .build()

            val userKey = ParcelUuid(keysInteractor.getCurrentUserKey().deviceKey)

            val serviceData = AdvertiseData.Builder().let { builder ->
                builder.addServiceUuid(userKey)
                builder.build()
            }

            val scanResponseData = AdvertiseData.Builder().let { builder ->
                builder.addManufacturerData(trackerManufacturerId, trackerManufactureData)
                builder.build()
            }

            advertiser.startAdvertising(
                settings,
                serviceData,
                scanResponseData,
                callback
            )
        }
    }

    override fun stopBroadcasting() {
        if (::callback.isInitialized) {
            advertiser.stopAdvertising(callback)
        }
        disposables.clear()
    }

    private fun getCallback(
        emitter: CompletableEmitter
    ): AdvertiseCallback {
       return object : AdvertiseCallback() {
           override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
               super.onStartSuccess(settingsInEffect)
               emitter.onComplete()
           }

           override fun onStartFailure(errorCode: Int) {
               super.onStartFailure(errorCode)
               emitter.onError(IllegalStateException("$errorCode"))
           }
       }
    }

    private lateinit var callback: AdvertiseCallback
}