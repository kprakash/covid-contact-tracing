package com.kartik.covidtracker.service.keys

import com.kartik.covidtracker.data.models.DeviceKey
import com.kartik.covidtracker.data.repositories.keys.DeviceKeysDao
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.util.*

interface DeviceKeysInteractor {
    fun getCurrentUserKey(): DeviceKey
    fun cleanupExpiredKeys()

    fun getAllKeys(createdDurationFromNow: Duration): List<DeviceKey>
    fun keyChangeListener(): Observable<Unit>
}

class DeviceKeysInteractorImpl(
    private val repo: DeviceKeysDao
) : DeviceKeysInteractor {
    private lateinit var currentUserKey: DeviceKey
    private val currentKeyExpiration = Duration.ofHours(24)
    private val allKeysExpiration = Duration.ofDays(15)

    private val keyUpdateSubject = BehaviorSubject.create<Unit>()

    override fun getCurrentUserKey(): DeviceKey {
        val now = Instant.now()
        return if (::currentUserKey.isInitialized) {
            if (Duration.between(currentUserKey.createdAt, now) > currentKeyExpiration) {
                generateNewKey(now)
            } else {
                currentUserKey
            }
        } else {
            if (fetchNonExpiredKey()) {
                currentUserKey
            } else {
                generateNewKey(now)
            }

        }
    }

    override fun cleanupExpiredKeys() {
        val now = Instant.now()
        repo.readAll()
            .filter { Duration.between(it.createdAt, now) > allKeysExpiration }
            .forEach { repo.delete(it) }
    }

    override fun getAllKeys(createdDurationFromNow: Duration): List<DeviceKey> {
        getCurrentUserKey()
        val now = Instant.now()
        return repo.readAll()
            .filter { Duration.between(it.createdAt, now) <= createdDurationFromNow }
    }

    override fun keyChangeListener(): Observable<Unit> {
        return keyUpdateSubject.hide()
    }

    private fun fetchNonExpiredKey(): Boolean {
        val now = Instant.now()
        repo.getLatestKey()?.let {
            if (Duration.between(it.createdAt, now) < currentKeyExpiration) {
                currentUserKey = it
                return true
            }
        }
        return false
    }

    private fun generateNewKey(now: Instant): DeviceKey {
        val key = UUID.randomUUID()
        return DeviceKey(key, now).also {
            currentUserKey = it
            repo.save(it)
        }
    }
}