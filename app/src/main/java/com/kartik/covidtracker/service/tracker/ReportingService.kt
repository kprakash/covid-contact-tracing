package com.kartik.covidtracker.service.tracker

import com.kartik.covidtracker.data.repositories.covid.CovidUserResultsDao
import com.kartik.covidtracker.data.repositories.firestore.CloudFirestore
import com.kartik.covidtracker.service.keys.DeviceKeysInteractor
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.Duration
import java.util.concurrent.TimeUnit

interface ReportingService {
    fun startService()
    fun stopService()

    fun forceSync(): Completable
}

class ReportingServiceImpl(
    private val dao: CovidUserResultsDao,
    private val database: CloudFirestore,
    private val keysInteractor: DeviceKeysInteractor
) : ReportingService {
    private val disposables = CompositeDisposable()

    override fun startService() {
        uploadCovidUserDetails()
            .delay(TimeUnit.HOURS.toMillis(12), TimeUnit.HOURS)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .retry()
            .repeat()
            .subscribe()
            .also { disposables.add(it) }
    }

    override fun stopService() {
        disposables.clear()
    }

    override fun forceSync(): Completable {
        return uploadCovidUserDetails()
    }

    private fun uploadCovidUserDetails(): Completable {
        return Completable.create {
            val keys = keysInteractor.getAllKeys(Duration.ofDays(15))
                .map { it.deviceKey }

            dao.getLastSubmittedTestResult()?.let { result ->
                if (result.testResult) {
                    database.uploadTestResult(
                        keys = keys,
                        result = result
                    )
                }
            }

            dao.getSymptomLastSubmittedValue()?.let { symptoms ->
                database.uploadSymptoms(
                    keys = keys,
                    symptoms = symptoms
                )
            }
            it.onComplete()
        }
    }
}