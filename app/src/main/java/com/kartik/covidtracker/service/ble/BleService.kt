package com.kartik.covidtracker.service.ble

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.kartik.covidtracker.service.keys.DeviceKeysInteractor
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

interface BleService {
    var isRunning: Boolean
    fun startBleServices(context: Context)
    fun stopBleServices()
}

class BleServiceImpl(
    private val advertiser: BleBroadcaster,
    private val scanner: BleScanner
) : BleService {
    private val disposables = CompositeDisposable()

    override var isRunning: Boolean = false

    override fun startBleServices(context: Context) {
        setupAdapterListeners(context)
        if (BluetoothAdapter.getDefaultAdapter().isEnabled) {
            startBleProcess()
        }
    }

    override fun stopBleServices() {
        stopBleProcess()
    }

    private fun setupAdapterListeners(context: Context) {
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (intent?.action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                    when (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                        BluetoothAdapter.STATE_OFF -> {
                            stopBleProcess()
                        }
                        BluetoothAdapter.STATE_ON -> {
                            startBleProcess()
                        }
                        BluetoothAdapter.STATE_TURNING_OFF -> {
                            stopBleProcess()
                        }
                        BluetoothAdapter.STATE_TURNING_ON -> { }
                    }
                }
            }
        }

        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        context.registerReceiver(receiver, filter)
    }

    fun startBleProcess() {
        advertiser.startBroadcasting()
            .subscribeOn(Schedulers.io())
            .subscribe()
            .also {
                isRunning = true
                disposables.add(it)
            }

        scanner.startScanning()
    }

    fun stopBleProcess() {
        disposables.clear()
        advertiser.stopBroadcasting()
        scanner.stopScanning()
        isRunning = false
    }
}