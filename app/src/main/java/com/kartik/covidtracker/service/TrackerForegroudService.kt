package com.kartik.covidtracker.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import com.kartik.covidtracker.service.ble.BleService
import com.kartik.covidtracker.service.tracker.ReportingService
import org.koin.android.ext.android.inject

class TrackerForegroundService : Service() {

    private val bleService: BleService by inject()
    private val reportingService: ReportingService by inject()

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        makeForeground()
        bleService.startBleServices(this)
        reportingService.startService()
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        bleService.stopBleServices()
        reportingService.stopService()
    }

    private fun makeForeground() {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel()
            } else {
                ""
            }
        val notification = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder(this, channelId)
        } else {
            Notification.Builder(this)
        }

        notification
            .setContentTitle("COVID-19 Tracker Service is running")
            .setContentText("COVID-19 Tracker App Service")
            .build()
            .let { startForeground(ONGOING_NOTIFICATION_ID, it) }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String {
        val chan = NotificationChannel(
            CHANNEL_ID,
            CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return CHANNEL_ID
    }


    companion object {
        private const val ONGOING_NOTIFICATION_ID = 1
        private const val CHANNEL_ID = "ForegroundServiceChannel"
        private const val CHANNEL_NAME = "TrackerForegroundServiceChannel"
    }
}