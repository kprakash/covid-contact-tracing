package com.kartik.covidtracker.service.ble

import android.util.Log
import com.kartik.covidtracker.MainApplication
import com.kartik.covidtracker.data.models.Contact
import com.kartik.covidtracker.data.repositories.contacts.ContactsDao
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanResult
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.Instant
import java.lang.Exception
import java.util.UUID

interface BleScanner {
    fun startScanning()
    fun stopScanning()
}

class BleScannerImpl(
    private val contactsDao: ContactsDao
) : BleScanner {

    private val disposables = CompositeDisposable()

    override fun startScanning() {
        val bleClient = RxBleClient.create(MainApplication.instance())
        val settings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
            .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
            .build()

        val filter = ScanFilter.Builder().let { builder ->
            builder.setManufacturerData(trackerManufacturerId, trackerManufactureData)
            builder.build()
        }

        bleClient.scanBleDevices(settings, filter)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .retry()
            .doOnError { }
            .subscribe {
                onDeviceEncountered(it)
            }.also { disposables.add(it) }

    }

    override fun stopScanning() {
        disposables.clear()
    }

    private fun onDeviceEncountered(result: ScanResult) {
        val userId = result.scanRecord.serviceUuids?.first()?.toString() ?: return

        try {
            UUID.fromString(userId)
        } catch (e: Exception) {
            return
        }

        val contact = Contact(
            deviceId = userId,
            lastContactedTime = Instant.now(),
            rssi = result.rssi
        )
        contactsDao.save(contact)
    }
}